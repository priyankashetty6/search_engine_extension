// Copyright (c) 2011 The Chromium Authors. All rights reserved.
function $(id) {
  return document.getElementById(id);
}

// In order to keep the popup bubble from shrinking as your search widens, we set an explicit width on the outermost div.
var didSetExplicitWidth = false;

function adjustWidthIfNeeded(filter) {
  if (filter.length > 0 && !didSetExplicitWidth) {
    // Set an explicit width, correcting for any scroll bar present.
    var outer = $('outer');
    var correction = window.innerWidth - document.documentElement.clientWidth;
    var width = outer.offsetWidth;
    $('spacer_dummy').style.width = width + correction + 'px';
    didSetExplicitWidth = true;
  }
}


function onSearchInput() {
  var filter = $('search').value;
  adjustWidthIfNeeded(filter);
}


// Initalize the popup window.
document.addEventListener('DOMContentLoaded', function () {
  chrome.management.getAll(function(info) {
	onSearchInput();
      });
     
//on click Opens the search result in a new tab.
  $('submitButton').addEventListener('input', onSearchInput);
	var link=document.getElementById('submitButton');
	link.addEventListener('click', function() {
	var newURL = "http://google.com/";
	if($('search').value!=null){
		newURL = "http://google.com/search?q="+$('search').value;
	}
        chrome.tabs.create({ 
		url: newURL
	});
	window.close();
    });
    
});
 
